package com.socket;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ClientApplication {

	public static SocketClient client;
	public static Thread clientThread;
	public int port;
	public String serverAddr, username, password;

	public File file;
	public String historyFile = "D:/History.xml";
	public History hist;

	public static void main(String[] args) {

		ClientApplication app = new ClientApplication();
		app.serverAddr = "localhost";
		app.port = 13000;
		app.username = "sarm";
		app.password = "123";
		
		// call server
		try {
			client = new SocketClient(app);
			clientThread = new Thread(client);
			clientThread.start();
			client.send(new Message("test", "testUser", "testContent", "SERVER"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// login
		app.loginClient();
		
		//send massage
		app.sendMassage();
		
		
		
	}
	
	void loginClient() {
		Scanner sc = new Scanner(System.in);
		
//	    System.out.println("Enter username");
//	    username = sc.nextLine();  // Read user input
//	    
//	    System.out.println("Enter password");
//	    password = sc.nextLine();  // Read user input
		
		if(!username.isEmpty() && !password.isEmpty()){
            client.send(new Message("login", username, password, "SERVER"));
        }
	}
	
	void sendMassage() {
		Scanner sc = new Scanner(System.in);
		String target = "All";
		String msg = "";
		while(msg != "exit") {
			System.out.print("Msg: ");
		    msg = sc.nextLine();
		    if(!msg.isEmpty()){
	            client.send(new Message("message", username, msg, target));
	        }
		}
        
	}

}
