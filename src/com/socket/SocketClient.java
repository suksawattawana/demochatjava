package com.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Date;

public class SocketClient implements Runnable {

	public int port;
	public String serverAddr;
	public Socket socket;
	public ObjectInputStream In;
	public ObjectOutputStream Out;
	public History hist;
	ClientApplication app;

//  ----------------------------------------------
	String username;

	public SocketClient(ClientApplication app) throws IOException {
		
		// set data
		this.app = app;
		this.serverAddr = app.serverAddr;
		this.port = app.port;
		socket = new Socket(InetAddress.getByName(serverAddr), port);

		Out = new ObjectOutputStream(socket.getOutputStream());
		Out.flush();
		In = new ObjectInputStream(socket.getInputStream());

		System.out.println("socket: " + socket);

		hist = app.hist;
	}

	public void send(Message msg) {
		try {
			Out.writeObject(msg);
			Out.flush();

			if (msg.type.equals("message") && !msg.content.equals(".bye")) {
				String msgTime = (new Date()).toString();
				try {
					System.out.println(new Object[] { "Me", msg.content, msg.recipient, msgTime });
				} catch (Exception ex) {
				}
			}
		} catch (IOException ex) {
			System.out.println("Exception SocketClient send()");
		}
	}

	@Override
	public void run() {
		boolean keepRunning = true;
		while (keepRunning) {
			try {
				Message msg = (Message) In.readObject();

				if (msg.type.equals("message")) {
					if (msg.recipient.equals(username)) {
						System.out.println("[" + msg.sender + " > Me] : " + msg.content);
					} else {
						System.out.println("[" + msg.sender + " > " + msg.recipient + "] : " + msg.content + "\n");
					}
				} else if (msg.type.equals("login")) {
					if (msg.content.equals("TRUE")) {
						System.out.println("[SERVER > Me] : Login Successful\n");
					} else {
						System.out.println("[SERVER > Me] : Login Failed\n");
					}
				} else {
					System.out.println("[SERVER > Me] : Unknown message type\n");
				}
			} catch (Exception ex) {
				keepRunning = false;
				System.out.println("[Application > Me] : Connection Failure\n");
				app.clientThread.stop();
				ex.printStackTrace();
			}
		}
	}

}
