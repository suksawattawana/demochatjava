package com.tcp_concurrent_java_master;

//TCPConcurrentServer.java
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer {
	ServerSocket welcomeSocket = null;
	int port = 8080;
	
	public static void main(String argv[]) {
		TCPServer app = new TCPServer();
		app.startServer();
	}
	
	void startServer() {
		try {
			welcomeSocket = new ServerSocket(port);
		} catch (IOException e) {
			System.exit(1);
		}
		
		while (true) {
			try {
				System.out.println("The server is waiting ");
				Socket connectionSocket = welcomeSocket.accept();
				
				ServerThread serverThread = new ServerThread(connectionSocket);
				serverThread.start();
				
			} catch (IOException e) {
				System.out.println("Cannot create this connection");
			}
		}
	}
}
