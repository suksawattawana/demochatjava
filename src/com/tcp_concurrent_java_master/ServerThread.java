package com.tcp_concurrent_java_master;

//EchoThread.java
import java.io.*;
import java.net.*;
import java.util.*;

public class ServerThread extends Thread {
	private Socket connectionSocket;

	public ServerThread(Socket connectionSocket) {
		this.connectionSocket = connectionSocket;
	}

	public void run() {
		Scanner inFromClient = null;
		DataOutputStream outToClient = null;
		try {
			inFromClient = new Scanner(connectionSocket.getInputStream());
			outToClient = new DataOutputStream(connectionSocket.getOutputStream());
			String data = inFromClient.nextLine();
			String newData = data.toUpperCase() + '\n';
			outToClient.writeBytes(newData);

		} catch (IOException e) {
			System.err.println("Closing Socket connection");
		} finally {
			try {
				if (inFromClient != null)
					inFromClient.close();
				if (outToClient != null)
					outToClient.close();
				if (connectionSocket != null)
					connectionSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
