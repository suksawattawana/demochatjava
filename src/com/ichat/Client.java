package com.ichat;

//Java implementation for a client 
//Save file as Client.java 
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

//Client class 
public class Client implements Runnable {

	Socket socket = null;
	DataInputStream dis = null;
	DataOutputStream dos = null;

	public Client() throws UnknownHostException, IOException {
		// connection with server port 8080
		socket = new Socket(InetAddress.getByName("localhost"), 8080);

		// input and out streams
		dis = new DataInputStream(socket.getInputStream());
		dos = new DataOutputStream(socket.getOutputStream());
		
		Thread thread = new Thread(this);
		thread.start();

		sendMsg();
	}

	void sendMsg() {
		@SuppressWarnings("resource")
		Scanner scn = new Scanner(System.in);
		try {

			String msg = null;
			while (true) {
				System.out.print("enter your msg: ");
				msg = scn.nextLine();
				dos.writeUTF(msg);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		boolean keepRunning = true;
		while (keepRunning) {
			try {
				String msg = dis.readUTF();
				System.out.println("other: " + msg);
			} catch (Exception e) {
				keepRunning = false;
			}
		}

	}

	public static void main(String[] args) throws IOException {
		new Client();
	}

}
