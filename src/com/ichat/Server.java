package com.ichat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

//Server class 
public class Server {

	ClientHandler clients[] = new ClientHandler[50];
	ServerSocket server = null;
	Socket socket = null;
	int clientCount = 0;

	public Server() {
		try {
			// server is listening on port 5056
			server = new ServerSocket(8080);

			// running infinite loop for getting
			// start();
			while (true) {
				try {
					System.out.println("Waiting for a client ...");
					socket = server.accept();
					System.out.println("A new client is connected : " + socket);

					// create a new thread object
					addClientHandler(socket);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
		}
	}

	void addClientHandler(Socket socket) throws IOException {
		System.out.println("Client accepted: " + socket);
		clients[clientCount] = new ClientHandler(this, socket);
		clients[clientCount].open();
		clients[clientCount].start();
		clientCount++;
	}

	public void handle(int ID, String data) {
		System.out.println("clientCount: " + clientCount);
		for (int i = 0; i < clientCount; i++) {

			System.out.println("sand msg to " + clients[i].ID + " : " + data);
			clients[i].send(data);
		}
	}

	public static void main(String[] args) throws IOException {
		// start app
		new Server();
	}

}

//ClientHandler class ����Ѻ�Ѵ��� client
class ClientHandler extends Thread {

	DataInputStream inputFormClient;
	DataOutputStream outputToClient;
	Socket socket;
	Server server;
	int ID = -1;

	// Constructor
	public ClientHandler(Server server, Socket s) {
		super();
		this.server = server;
		this.socket = s;
		this.ID = s.getPort();
	}

	@Override
	public void run() {
		while (true) {
			try {
				server.handle(ID, inputFormClient.readUTF());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void open() throws IOException {
		inputFormClient = new DataInputStream(socket.getInputStream());
		outputToClient = new DataOutputStream(socket.getOutputStream());
	}

	public void send(String msg) {
		try {
			outputToClient.writeUTF(msg);
		} catch (IOException ex) {
			System.out.println("Exception [SocketClient : send(...)]");
		}
	}
}
